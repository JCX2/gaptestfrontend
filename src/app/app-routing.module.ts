import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponentComponent} from '../app/login-component/login-component.component';
import {AppComponent} from '../app/app.component';
import {InsurancesComponent} from '../app/insurances/insurances.component'

const routes: Routes = [
{path:'',
component:AppComponent,
children:[
  {
    path:'',component:LoginComponentComponent
  },
  {
    path:'insurances',component:InsurancesComponent
  }
]
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
