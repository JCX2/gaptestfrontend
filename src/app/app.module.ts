import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponentComponent } from './login-component/login-component.component';
import { MyMaterialModule } from  './material.module'
// import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {Globals} from '../app/global';
import { PrincipalComponent } from './principal/principal.component';
import { InsurancesComponent } from './insurances/insurances.component';

import {AddDialogComponent} from './dialogs/add/add.dialog.component';
import {EditDialogComponent} from './dialogs/edit/edit.dialog.component';
import {DeleteDialogComponent} from './dialogs/delete/delete.dialog.component';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material';
import {DataService} from './services/data.service';
import {DateFormatPipe} from './pipes/date-format/dateFormat.pipe'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponentComponent,
    PrincipalComponent,
    InsurancesComponent,AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent,
    DateFormatPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MyMaterialModule,
    HttpClientModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatSortModule,
    MatTableModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatSelectModule,
    MatDatepickerModule
    
  ],
  entryComponents: [
    AddDialogComponent,
    EditDialogComponent,
    DeleteDialogComponent
  ],
  providers: [Globals,DataService,DateFormatPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
