import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from 'protractor';
import { Globals } from '../global';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {

  constructor(private http: HttpClient, private endpoints: Globals) { }
  name: string;
  pass: string;
  ngOnInit() {
  }

  login() {

    this.http.post(this.endpoints.endpointToken, { "name": this.name, "password": this.pass }).subscribe(
      (response) => {
        localStorage.setItem("currentUser",JSON.stringify(response))
        console.log(response);
      },
      (err) => {
        console.log(err);
      }
    )
  }
}
