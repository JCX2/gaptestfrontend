import { Injectable } from "@angular/core";


@Injectable()
export class Globals {
   domain="http://localhost:49906"
   endpointToken = this.domain+'/api/auth/usertoken';
   endpointInsurances =this.domain+'/api/insurance/allInsurances';
   endpointInsurancesTypes=this.domain+'/api/insurance/allInsurancesTypes';
   endpointInsuranceCreate=this.domain+'/api/insurance/add';
   

}