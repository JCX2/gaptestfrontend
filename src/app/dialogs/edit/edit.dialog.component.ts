import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
import {DataService} from '../../services/data.service';
import {FormControl, Validators} from '@angular/forms';
import { Globals } from 'src/app/global';
import { HttpClient } from '@angular/common/http';
import { InsurancesType } from 'src/app/models/models';

@Component({
  selector: 'app-baza.dialog',
  templateUrl: '../../dialogs/edit/edit.dialog.html',
  styleUrls: ['../../dialogs/edit/edit.dialog.css']
})
export class EditDialogComponent {
  _insurancesTypes: InsurancesType[];
  _idSeleccionado: string;
  constructor(public dialogRef: MatDialogRef<EditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, public dataService: DataService, public endpoints:Globals, private httpClient: HttpClient) {
                this.obtenerDatosTiposPoliza();
               }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }
  obtenerDatosTiposPoliza() {
    this.httpClient.get(this.endpoints.endpointInsurancesTypes).subscribe(data => {
      //  console.log(data)
      this._insurancesTypes = (data as InsurancesType[]);
      //  console.log(this._insurancesTypes);
    },
      err => {
        console.log(err);
      }
    );

  }
  submit() {
    // emppty stuff
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  stopEdit(): void {
    this.dataService.updateIssue(this.data);
  }
}
