import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Component, Inject } from '@angular/core';
import { DataService } from '../../services/data.service';
import { FormControl, Validators } from '@angular/forms';
import { Insurances, InsurancesType } from '../../models/models';
import { HttpClient } from '@angular/common/http';
import { Globals } from 'src/app/global';
import { DateFormatPipe } from 'src/app/pipes/date-format/dateFormat.pipe';

@Component({
  selector: 'app-add.dialog',
  templateUrl: '../../dialogs/add/add.dialog.html',
  styleUrls: ['../../dialogs/add/add.dialog.css']
})

export class AddDialogComponent {

  _insurancesTypes: InsurancesType[] = [];
  _idSeleccionado: string;
  constructor(public dialogRef: MatDialogRef<AddDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Insurances,
    public dataService: DataService, private httpClient: HttpClient, private endpoints: Globals , private dateFormatPipe: DateFormatPipe) {
    this.obtenerDatosTiposPoliza();
  }

  formControl = new FormControl('', [
    Validators.required
    // Validators.email,
  ]);

  getErrorMessage() {
    return this.formControl.hasError('required') ? 'Required field' :
      this.formControl.hasError('email') ? 'Not a valid email' :
        '';
  }



  obtenerDatosTiposPoliza() {
    this.httpClient.get(this.endpoints.endpointInsurancesTypes).subscribe(data => {
      //  console.log(data)
      this._insurancesTypes = (data as InsurancesType[]);
      //  console.log(this._insurancesTypes);
    },
      err => {
        console.log(err);
      }
    );

  }

  save() {
    this.data.id_insurance_type = this._idSeleccionado;
    this.data.intialDate = this.dateFormatPipe.transform(this.data.intialDate);
    this.httpClient.post<Insurances>(this.endpoints.endpointInsuranceCreate, this.data).subscribe(
      response => {
        if ((response as any).code === 0) {
          this.confirmAdd();
        }
      }
    );

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  public confirmAdd(): void {
    
    this.dataService.addIssue(this.data);
  }
}
