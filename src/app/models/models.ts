export class Customers {
    name: string
    phone: string
    email: string
    identificationCode: string
    CustomerId: string
    insurances: Insurances[] = []
}

export class InsurancesType {
    name: string
    percentage: 0
    InsurancesTypeId: string
}

export class Insurances {
    customers: Customers[] = []
    insurancesType: InsurancesType = new InsurancesType()
    name: string
    description: string
    intialDate: string
    insuranceTime: 0
    price: 0
    risk: string    
    InsuranceId: string
    id_insurance_type: string
}